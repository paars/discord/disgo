package disgo

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
)

const zeroSpace = "​" // <- Zero width space

var (
	reDiscordSpecialChars = regexp.MustCompile(`([*_~` + "`" + `<])`)
	reChannelMention      = regexp.MustCompile(`^\s*<#(\d+)>\s*$`)
)

func Mention(u *discordgo.User) string {
	return fmt.Sprintf("<@%v>", u.ID)
}

func GetRole(rID string, roles []*discordgo.Role) *discordgo.Role {
	for _, r := range roles {
		if r.ID == rID {
			return r
		}
	}

	return nil
}

func GetUser(uID string, users []*discordgo.User) *discordgo.User {
	for _, u := range users {
		if u.ID == uID {
			return u
		}
	}

	return nil
}

func QuoteMarkdown(msg string) string {
	return reDiscordSpecialChars.ReplaceAllString(msg, "\\$1")
}

func EscapeEveryoneMention(in string) string {
	s := strings.Replace(in, "@everyone", "@"+zeroSpace+"everyone", -1)
	s = strings.Replace(s, "@here", "@"+zeroSpace+"here", -1)
	return s
}

func GetAllGuildMembers(session *discordgo.Session, guildID string) ([]*discordgo.Member, error) {
	g, err := session.State.Guild(guildID)
	if err != nil {
		return nil, err
	}
	if g.MemberCount == len(g.Members) {
		return g.Members, nil
	}

	var after string
	members := make([]*discordgo.Member, 0)

	for {
		resp, err := session.GuildMembers(guildID, after, 1000)
		if err != nil {
			return nil, err
		}
		members = append(members, resp...)

		if len(resp) < 1000 {
			break // Reached the end
		}

		after = members[len(members)-1].User.ID
	}
	return members, nil
}

func GetRoleByName(roles []*discordgo.Role, name string) *discordgo.Role {
	name = strings.ToLower(name)
	for _, r := range roles {
		if strings.ToLower(r.Name) == name {
			return r
		}
	}
	return nil
}

func HasRole(member *discordgo.Member, roleID string) bool {
	for _, rID := range member.Roles {
		if rID == roleID {
			return true
		}
	}
	return false
}

func HasRoleByName(member *discordgo.Member, roles []*discordgo.Role, name string) bool {
	r := GetRoleByName(roles, name)

	if r == nil {
		return false
	}

	for _, rid := range member.Roles {
		if rid == r.ID {
			return true
		}
	}

	return false
}
