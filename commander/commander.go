package commander

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/andersfylling/disgord"
	"go.uber.org/zap"
)

type commandList = map[string]Command

// Commander command handler for disgord
type Commander struct {
	Logger   *zap.Logger
	commands commandList
	client   *disgord.Client
}

// New Commander instance
func New(client *disgord.Client) *Commander {
	cmder := &Commander{
		Logger:   zap.NewNop(),
		client:   client,
		commands: make(commandList),
	}
	return cmder
}

// Add a new command
func (cmder *Commander) Add(command string, handler Command) {
	cmder.commands[command] = handler
}

// Handler middleware for disgord
func (cmder *Commander) Handler(_ disgord.Session, e *disgord.MessageCreate) {
	logger := cmder.Logger.With(zap.String("ctx", e.Message.ID.String()))
	logger.Debug("incoming message")

	// recover from panics
	defer func() {
		if r := recover(); r != nil {
			err, ok := r.(error)
			if !ok {
				err = fmt.Errorf("unkown error happened: %v", r)
			}

			logger.Error("recovered from panic",
				zap.Error(err),
				zap.String("msg", e.Message.Content),
				zap.Stack("stack"))

			cmder.client.SendMsg(context.Background(),
				e.Message.ChannelID,
				fmt.Sprintf("Bot is panicking, contact the bot owner! Context-ID: `%v`", e.Message.ID),
			)
		}
	}()

	// force channel existance
	_, err := cmder.client.GetChannel(context.Background(), e.Message.ChannelID)
	if err != nil {
		logger.Warn("failed to fetch channel", zap.Error(err))
		return
	}

	// Check if any additional fields were provided to the command, if not just run the default command if possible
	if e.Message.Content == "" {
		return
	}

	// run command
	line := strings.SplitN(e.Message.Content, " ", 2)
	logger.Debug("searching command", zap.String("cmd", line[0]))
	cmd, found := cmder.commands[line[0]]
	if !found {
		logger.Info("unkown command requested", zap.String("cmd", line[0]))
		return
	}
	if len(line) != 2 {
		line = append(line, "")
	}

	start := time.Now()
	err = cmd(cmder.client, line[1], e.Message)
	l := logger.With(zap.Duration("time", time.Now().Sub(start)))
	if err != nil {
		l.Warn("failed to execute command", zap.Error(err))
		return
	}

	l.Info("executed command", zap.String("cmd", line[0]))
}
