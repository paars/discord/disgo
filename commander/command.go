package commander

import (
	"github.com/andersfylling/disgord"
)

// Command handler
type Command func(c *disgord.Client, args string, m *disgord.Message) error
