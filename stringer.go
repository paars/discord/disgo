package disgo

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func Guild(g *discordgo.Guild) string {
	if g == nil {
		return "<nil>"
	}

	return fmt.Sprintf("<Guild %v [%v]>", g.ID, g.Name)
}

func Channel(c *discordgo.Channel) string {
	if c == nil {
		return "<nil>"
	}

	return fmt.Sprintf("<Channel %v [%v]>", c.ID, c.Name)
}

func User(u *discordgo.User) string {
	if u == nil {
		return "<nil>"
	}

	return fmt.Sprintf("<User %v [%v#%v]>", u.ID, u.Username, u.Discriminator)
}
