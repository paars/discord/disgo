package disgo

import (
	"sort"

	"github.com/bwmarrin/discordgo"
)

type Permission int

const (
	PermissionCreateInstantInvite Permission = 0x00000001
	PermissionKickMembers                    = 0x00000002
	PermissionBanMembers                     = 0x00000004
	PermissionAdministrator                  = 0x00000008
	PermissionManageChannels                 = 0x00000010
	PermissionManageGuild                    = 0x00000020
	PermissionAddReactions                   = 0x00000040
	PermissionViewAuditLog                   = 0x00000080
	PermissionViewChannel                    = 0x00000400
	PermissionSendMessages                   = 0x00000800
	PermissionSendTTSMessages                = 0x00001000
	PermissionManageMessages                 = 0x00002000
	PermissionEmbedLinks                     = 0x00004000
	PermissionAttachFiles                    = 0x00008000
	PermissionReadMessageHistory             = 0x00010000
	PermissionMentionEveryone                = 0x00020000
	PermissionUseExternalEmojis              = 0x00040000
	PermissionConnect                        = 0x00100000
	PermissionSpeak                          = 0x00200000
	PermissionMuteMembers                    = 0x00400000
	PermissionDeafenMembers                  = 0x00800000
	PermissionMoveMembers                    = 0x01000000
	PermissionUseVAD                         = 0x02000000
	PermissionChangeNickname                 = 0x04000000
	PermissionManageNicknames                = 0x08000000
	PermissionManageRoles                    = 0x10000000
	PermissionManageWebhooks                 = 0x20000000
	PermissionManageEmojis                   = 0x40000000
)

func HasPermission(requestedPermission Permission, flags int) bool {
	return flags&int(requestedPermission) == int(requestedPermission)
}

// TODO: channel specific overrides
func IsMemberAllowed(s *discordgo.Session, u *discordgo.User, g *discordgo.Guild, perm Permission) bool {
	m, err := s.State.Member(g.ID, u.ID)
	if err != nil {
		return false
	}

	if m == nil {
		m, err = s.GuildMember(g.ID, u.ID)
	}
	if err != nil {
		return false
	}

	// sort global roles
	roles := Roles(g.Roles)
	sort.Sort(roles)

	for _, r := range m.Roles {
		role := GetRole(r, roles)
		if role != nil && HasPermission(perm, role.Permissions) {
			return true
		}
	}

	return false
}
