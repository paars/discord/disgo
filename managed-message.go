package disgo

import (
	"sync"

	"github.com/bwmarrin/discordgo"
)

type ManagedMessage struct {
	m        *discordgo.Message
	c        string
	s        *discordgo.Session
	color    int
	headline string
	embed    *discordgo.MessageEmbed

	mux sync.Mutex
}

func NewManagedMessage(s *discordgo.Session, chID string, color int, headline string) (m *ManagedMessage, err error) {
	m = &ManagedMessage{s: s, c: chID, color: color, headline: headline}

	m.m, err = s.ChannelMessageSendEmbed(chID, &discordgo.MessageEmbed{
		Color: color,
		Title: headline,
	})

	if err != nil {
		return nil, err
	}

	s.AddHandler(m.onMessageCreate)

	return
}

func (m *ManagedMessage) onMessageCreate(_ *discordgo.Session, e *discordgo.MessageCreate) {
	m.mux.Lock()
	defer m.mux.Unlock()

	if e.ChannelID == m.c && e.Author.ID != m.s.State.User.ID && !e.Author.Bot {
		m.s.ChannelMessageDelete(e.ChannelID, m.m.ID)
		m.m, _ = m.s.ChannelMessageSendEmbed(e.ChannelID, m.embed)
	}
}

func (m *ManagedMessage) UpdateMessage(embed *discordgo.MessageEmbed) (err error) {
	m.mux.Lock()
	defer m.mux.Unlock()

	embed.Color = m.color
	embed.Title = m.headline
	m.embed = embed
	_, err = m.s.ChannelMessageEditEmbed(m.c, m.m.ID, embed)

	return
}

func (m *ManagedMessage) UpdateChannel(channel string) (err error) {
	m.mux.Lock()
	defer m.mux.Unlock()

	m.s.ChannelMessageDelete(m.c, m.m.ID)
	m.c = channel
	m.m, err = m.s.ChannelMessageSendEmbed(m.c, m.embed)

	return
}
