# disgo : a discordgo helper library

## Overview [![GoDoc](https://godoc.org/gitlab.com/paars/discord/disgo?status.svg)](https://godoc.org/gitlab.com/paars/discord/disgo) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/paars/discord/disgo)](https://goreportcard.com/report/gitlab.com/paars/discord/disgo) [![Sourcegraph](https://sourcegraph.com/gitlab.com/paars/discord/disgo/-/badge.svg)](https://sourcegraph.com/gitlab.com/paars/discord/disgo?badge)

disgo is a small discordgo helper library providing useful helper functions.

Also included is a commander library for an easy way to implement commands.

## Install

```
go get gitlab.com/paars/discord/disgo
```

## Contributing

[Guidelines](CONTRIBUTING.md)

## Contributors
Sorted alphabetically

* **[CKing](https://gitlab.com/cking)**
  * Main Developer

## Author

By [CKing / Kura](https://gitlab/cking), [@kura@niu.moe](https://niu.moe/@kura)

## License

[Apache 2.0](LICENSE)

