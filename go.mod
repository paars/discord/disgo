module gitlab.com/paars/discord/disgo

go 1.13

require (
	github.com/andersfylling/disgord v0.16.3
	github.com/bwmarrin/discordgo v0.20.2
	github.com/jonas747/dshardmanager v0.0.0-20180911185241-9e4282faed43
	github.com/rs/zerolog v1.17.2
	go.uber.org/zap v1.13.0
)
