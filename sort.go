package disgo

import (
	"strconv"

	"github.com/bwmarrin/discordgo"
)

func IsRoleAbove(a, b *discordgo.Role) bool {
	if a.Position != b.Position {
		return a.Position > b.Position
	}

	if a.ID == b.ID {
		return false
	}

	pa, _ := strconv.ParseInt(a.ID, 10, 64)
	pb, _ := strconv.ParseInt(b.ID, 10, 64)

	return pa < pb
}

func IsChannelAbove(a, b *discordgo.Channel) bool {
	if a.Position != b.Position {
		return a.Position > b.Position
	}

	if a.ID == b.ID {
		return false
	}

	pa, _ := strconv.ParseInt(a.ID, 10, 64)
	pb, _ := strconv.ParseInt(b.ID, 10, 64)

	return pa < pb
}

type Channels []*discordgo.Channel

func (r Channels) Len() int {
	return len(r)
}

func (r Channels) Less(i, j int) bool {
	return r[i].Position < r[j].Position
}

func (r Channels) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

type Roles []*discordgo.Role

func (r Roles) Len() int {
	return len(r)
}

func (r Roles) Less(i, j int) bool {
	return IsRoleAbove(r[i], r[j])
}

func (r Roles) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}
