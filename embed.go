package disgo

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func NewEmbedField(key string, value interface{}) *discordgo.MessageEmbedField {
	return &discordgo.MessageEmbedField{Name: key, Inline: false, Value: fmt.Sprintf("%v", value)}
}

func NewInlineEmbedField(key string, value interface{}) *discordgo.MessageEmbedField {
	f := NewEmbedField(key, value)
	f.Inline = true
	return f
}
